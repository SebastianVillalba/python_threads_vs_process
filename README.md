# README #

En este ejemplo se demuestra el uso de Threading y Mulprocessing en Python.

### I/O Bound ###

* Llamadas a API
* Interacción con archivos
* Interacción con DB

### CPU Bound ###

* Ordenamiento de arrays
* Cálculos
* Uso de For, While

El interpreter de Python puede manejar solo un hilo cuando se trata de CPU Bound. Esto quiere decir que si el interpreter tiene que realizar uso de CPU puede atender de a un hilo sin importar la cantidad de hilos que hayan.
Cuando se trata de I/O Bound se recomienda utilizar Threads, ya que no hace uso de procesador y Python puede gestionarlos de manera concurrente.
Cuando es CPU Bound se recomienda utilizar Multriprocessing. De esta manera cada subproceso va a tener un interpreter, y Python puede trabajar de manera concurrente.

https://codigofacilito.com/articulos/gil-python

https://www.youtube.com/watch?v=On0k9VMN9bE

