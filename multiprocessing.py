import multiprocessing
import time
import threading

def countdown(number):
    # SIN COMPUTO
    # time.sleep(5)

    #CON COMPUTO 
    while number > 0: 
        number -=1

if __name__ == '__main__':
    count = 100000000

    start1 = time.time()
    countdown(count)
    print(f'Tiempo transcurrido sin thread ni process: {time.time() - start1 }')

    start2 = time.time()

    # CON THREADS
    # t1 = threading.Thread(target=countdown, args=(count,))

    # CON PROCESOS
    t1 = multiprocessing.Process(target=countdown, args=(count,))

    t1.start()
    t1.join()
    print(f'Tiempo transcurrido: {time.time() - start2 }')


    # CON THREADS
    start3 = time.time()
    # t2 = threading.Thread(target=countdown, args=(count,))
    # t3 = threading.Thread(target=countdown, args=(count,))

    # CON MULTIPROCESOS
    t2 = multiprocessing.Process(target=countdown, args=(count,))
    t3 = multiprocessing.Process(target=countdown, args=(count,))
    
    t2.start()
    t3.start()

    t2.join()
    t3.join()

    print(f'Tiempo transcurrido total: {time.time() - start3 }')